package fr.ap.main

class Operator {

    val plus: Char = '+'
    val minus: Char = '-'
    val multiply: Char = '*'
    val divide: Char = '/'

    private var current: Char = plus

    fun setPlus() {
        current = plus
    }

    fun setMinus() {
        current = minus
    }

    fun setMultiply() {
        current = multiply
    }

    fun setDivide() {
        current = divide
    }

    fun getCurrentOperator(): Char {
        return current
    }
}