package fr.ap.main

import java.math.BigDecimal

class Calculator {

    var lastInput: String = ""
    var operator: Operator = Operator()

    fun refresh(currentDisplay: String, anyButtonToString: String): String {
        return refineZero(currentDisplay).plus(anyButtonToString)
    }

    private fun refineZero(s: String): String {
        return if (s[0] == '0') s.substring(1) else s
    }

    fun equal(current: String): String {
        var result: BigDecimal? = null

        if (operator.getCurrentOperator() == operator.plus) {
            result = current.toBigDecimal() + lastInput.toBigDecimal()
        }
        if (operator.getCurrentOperator() == operator.minus) {
            result = lastInput.toBigDecimal() - current.toBigDecimal()
        }
        if (operator.getCurrentOperator() == operator.multiply) {
            result = current.toBigDecimal() * lastInput.toBigDecimal()
        }
        if (operator.getCurrentOperator() == operator.divide) {
            result = current.toBigDecimal() / lastInput.toBigDecimal()
        }

        return if (result!! > 9999999999999.toBigDecimal()) R.string.error.toString() else result.toString()
    }
}