package fr.ap.main

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Instantiation of a Calculator
 * */
var calculator = Calculator()

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /** Set display text to : */
        mainDisplay.text = "0"
    }

    override fun onClick(v: View?) {

        /** How catch which button is clicked : */
        val anyButton: Button? = v?.findViewById(v.id)

        mainDisplay.text =
            calculator.refresh(mainDisplay.text.toString(), anyButton?.text.toString())

        println(anyButton?.text.toString() + " is clicked")


        reset.setOnClickListener {
            println("Reset is clicked")
            mainDisplay.text = "0"
            calculator.lastInput = ""
        }

        plus.setOnClickListener {
            println("Plus is clicked")
            calculator.operator.setPlus()
            calculator.lastInput = mainDisplay.text.toString()
            mainDisplay.text = "0"
        }

        minus.setOnClickListener {
            println("Minus is clicked")
            calculator.operator.setMinus()
            calculator.lastInput = mainDisplay.text.toString()
            mainDisplay.text = "0"
        }

        divide.setOnClickListener {
            println("Divide is clicked")
            calculator.operator.setDivide()
            calculator.lastInput = mainDisplay.text.toString()
            mainDisplay.text = "0"
        }

        multiply.setOnClickListener {
            println("Multiply is clicked")
            calculator.operator.setMultiply()
            calculator.lastInput = mainDisplay.text.toString()
            mainDisplay.text = "0"
        }

        equal.setOnClickListener {
            println("Equal is clicked")
            mainDisplay.text = calculator.equal(mainDisplay.text.toString())

            //TODO have to test it :
//            if (calculator.operator.getCurrentOperator() == calculator.operator.minus) {
//                calculator.lastInput = mainDisplay.text.toString()
//            }
            errorTesting()
        }
    }

    private fun errorTesting() {
        if (mainDisplay.text.toString().toBigDecimal() > 999999999999.toBigDecimal() || mainDisplay.text.toString().toBigDecimal() == 999999999999.toBigDecimal()) {
            mainDisplay.text = R.string.error.toString()
        }
    }
}
